The helper script for final project of verilog course.
# Usage
```bash
$ python dump_img.py path_to_img_file

$ python img2tb.py ~/Desktop/example.jpg > autogen.v
$ cp autogen.v ../tb_autogen.v

$ python write.py ../final/image

$ python b2i.py 1010
10
```
