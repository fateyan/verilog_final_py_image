#!/usr/bin/env python
import os,sys
from PIL import Image

im = Image.open(sys.argv[1])

def lazyManHelper(val):
    return str(bin(val))[2:].zfill(8)

tpl = """
module tb_GrayScale();
    reg clk, Din;
    wire Dout;
    GrayScale gs(clk, Din, Dout);

    initial
    begin
        clk = 1'b0;
        repeat(%d) #25 clk = ~clk;
    end

    initial
    begin
        $dumpfile("simulation.vcd");
        $dumpvars;

        {{{placeholder}}}

        // nop
        #50 clk = 1'b0;
        #50 clk = 1'b0;
        #50 clk = 1'b0;
        #50 clk = 1'b0;
        #50 clk = 1'b0;
        #50 clk = 1'b0;

        #50
        $finish;
    end

endmodule
""" % (im.size[0]*im.size[1]*3*16+16)
stuff = ""
for i in range(0, im.size[0]):
    for j in range(0, im.size[1]):
        rgb = [lazyManHelper(x) for x in im.getpixel((i, j))]
        for k in range(0, 3):
            for l in range(0, 8):
                stuff += "Din = 1'b%s; \n#50\n" % (rgb[k][l])

tpl = tpl.replace("{{{placeholder}}}", stuff)
print tpl
