#!/usr/bin/env python
import os,sys
from PIL import Image

im = Image.new("RGB", (128, 128), (255, 255, 255))
p = im.load()

data = raw_input()

ptr = 0
for i in range(0, 128):
    for j in range(0, 128):
        r, g, b = [int(data[ptr: ptr+8], 2) for k in range(0, 3)]
        ptr += 8 

        #print r, g, b
        p[i, j] = (r, g, b)

im.save("out.png", "PNG")
im.close()
